const Result = require ('./Result');

const shortID = require('shortid');


class Room {
    
    constructor(){
        this.id = shortID.generate();
        this.numberOfPlayers =1;
        this.numberOfPlayersReady =0;
        this.numberOfPlayersReadyToBeSpawned =0;
        this.numberOfPlayersSpawned =0;
        this.time = Date.now();
        this.result = new Result();
        this.racefinished = false;
        this.roomTimer ;
    }

    async startGame( socket){
        this.time = Date.now();
        let roomId = this.id;
        let time = this.time;
        this.roomTimer = setInterval(function(){
            let timeData = {
                roomId : roomId,
                currentTime: (Date.now() - time) /1000
            }
            socket.to(roomId).emit("raceTimeChanged",timeData)
                 
        },10)
    }
    async stopTimer(){
            clearInterval(this.roomTimer);
    }
}

module.exports = Room;