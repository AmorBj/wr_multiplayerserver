
const shortID = require('shortid');
const Vector2 = require ('./Vector2');


class RaceData {
    constructor(){
        this.id = shortID.generate();     
        this.rank = 0;
        this.raceTime = 0;
        this.damageDone =0;
        this.healDone = 0;
        this.shieldDone = 0;
        this.items =[];
        this.exp = 0;
    }
}

module.exports = RaceData;