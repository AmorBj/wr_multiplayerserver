module.exports = class AnimationData{
    constructor(Speed=0 , IsGounded=true ,IsJumping=false){
        this.speed = Speed;
        this.isGrounded =IsGounded;
        this.isJumping =IsJumping;
    }
}