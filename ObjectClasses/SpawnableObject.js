

const shortID = require('shortid')
const Vector2 = require ('./Vector2');



class SpawnableObjects {
    constructor(){
        this.id = shortID.generate();
        this.name = 'spawnable Object';
        this.position = new Vector2();
        this.rotation = new Vector2();
        this.ownerId = '';
        this.amount = 0;
    }
}

module.exports = SpawnableObjects;