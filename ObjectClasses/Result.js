
const shortID = require('shortid');


class Result {
    constructor(){
        this.id = shortID.generate();
        this.players = [];
        this.playersFinished = 0;
        this.playersDead = 0;
    }
}

module.exports = Result;