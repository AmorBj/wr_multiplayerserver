
module.exports = class Vector2{
    constructor(X= 0, Y=0){
        this.x= X;
        this.y = Y;
    }

    normalize(){
        let norm = new Vector2();
        let mag = this.magnitude();
        norm.x = this.x / mag;
        norm.y = this.y/ mag ;
        return norm;
    }

    distance(otherPoint = Vector2){
        let distance = new Vector2();
        distance.x = otherPoint.x - this.x;
        distance.y = otherPoint.y - this.y;
        return distance.magnitude()
    }

    magnitude(){
        return Math.sqrt((this.x * this.x)(this.y * this.y));
    }

}