
const shortID = require('shortid');
const Vector2 = require ('./Vector2');
const RaceData = require ('./RaceData');


const AnimationData = require ('./AnimationData');
const HealthAndShield = require ('./HealthAndShield');


class Player {
    constructor(){
        this.id = shortID.generate();
        this.username='';
        this.position = new Vector2();
        this.faceRight = true;
        this.animData= new AnimationData();
        this.isReady = false;
        this.isAlive = true;
        this.racedata = new RaceData();
        
        this.status = 'online';
        // en principe nzid statu , in game , readytoplay ... , t3awenna 7atta fil les test mte3 leave w join etc
        // Health and shield params
        this.healthAndShield = new HealthAndShield();

        this.skin = {};
        this.dBUser = {}; 
    }
}

module.exports = Player;