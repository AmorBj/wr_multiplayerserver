const Promise = require('bluebird');
const mongoose = require('mongoose');



const QuestSchema = new mongoose.Schema({
    name: String,
    description :  String,
    // we can add reward Type Exp or Gold
    reward: Number,
    rewardType: {
        type :String,
        enum: ('Gold' , 'Exp'),
        default: 'Exp',
    },
    difficulty: String,
    requiredAmount:Number,
    questGoal : {
       type: String,
       enum : [ 'Damage' , 'Win' , 'Heal' , 'Use_Item' ],
    },
    endsIn : Number,
    mission: {
        type: String,
        enum : ['SINGLE_MATCH' ,'ANY'],
        default : "ANY",
    },
    createdAt: {
        type: Date,
        default: Date.now
    },

  });


  QuestSchema.statics = {
        /**
         * Get quest
         * @param {ObjectId} id - The objectId of quests.
      
         */
        get(id) {
        return this.findById(id)
            .exec()
            .then((quest) => {
            if (quest) {
                return quest;
            }
            
            });
        },
    
        /**
         * List quests in descending order of 'createdAt' timestamp.
         * @param {number} skip - Number of quests to be skipped.
         * @param {number} limit - Limit number of quests to be returned.
         * @returns {Promise<Quest[]>}
         */
        list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec();
        },
    }

  module.exports = mongoose.model('Quest', QuestSchema);