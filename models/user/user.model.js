const Promise = require('bluebird');
const mongoose = require('mongoose');
const Level = require('../Level/level.model')
const Skin = require('../Skin/skin.model')
const Quest = require('../Quest/quest.model')
const Item = require('../Item/item.model')



const ObjectId =  mongoose.Schema.Types.ObjectId;

/**
 * User Schema
 */
const UserSchema = new mongoose.Schema({
  username: {
    type: String,
    required: false,
    unique: true
  },
  fullName :  String,
  password: {
    type: String,
    required: false
  },
  mail:{
    type: String,
    unique:true,
    match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  },
  phone:{
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  },


  // here start game user atts
  gold : {
    type :Number,
    default : 5000 
  } ,
  experience: {
    type: Number ,
    default : 0
  },
  status :{
    type : String ,
    default: "horsligne"
  },
  level: { 
    type : mongoose.Schema.Types.ObjectId,
    ref : 'Level' ,
   } ,

  skins :[
    {
      element :{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'Skin'
      },  
      in_use: {
        type :Boolean,
        default : true, 
      },
      purchasedDate: {
        type :Date,
        default: Date.now
      }
    }
  ],
  quests: [ {
    currentQuest : { 
      type : mongoose.Schema.Types.ObjectId,
      ref : 'Quest'
     },
    takenAt: Date,
    currentAmount: Number
  }],
  
  items: [{
    currentItem: { 
      type : mongoose.Schema.Types.ObjectId,
      ref : 'Item'
     },
     purchasedDate: {
      type: Date,
      default: Date.now
    },
    currentPower:Number
  }],
  
  friends: [
    {
      friend: { 
        type : mongoose.Schema.Types.ObjectId,
        ref : 'User'
       },
       friendsFrom :{
         type: Date,
         default : Date.now
       }
    }
  ],
  
  
  facebookProvider: {
    id: String,
    token: String,
    displayName: String,
    name:
      { 
        familyName: String,
        givenName: String, 
        middleName: String },
    gender: String,
    emails: [ { value: String } ],
    photos:
    [ { value: String } ],
  },
  googleProvider: 
  {
    id: String,
    email: String,
    verified_email: Boolean,
    name: String,
    given_name: String,
    link: String,
    picture: String,
    gender: String,
    locale: String 
  }
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */

/**
 * Methods
 */
UserSchema.method({
});

/**
 * Statics
 */
UserSchema.statics = {
  /**
   * Get user
   * @param {ObjectId} id - The objectId of user.
   * @returns {Promise<User, APIError>}
   */
  get(id) {
    return this.findById(id)
      .populate('level')
      .populate('quests.currentQuest')
      .populate('items.currentItem')
      .populate('skins.element')
      .exec()
      .then((user) => {
        if (user) {
          return user;
        }
       
      });
  },

  /**
   * List users in descending order of 'createdAt' timestamp.
   * @param {number} skip - Number of users to be skipped.
   * @param {number} limit - Limit number of users to be returned.
   * @returns {Promise<User[]>}
   */
  list({ skip = 0, limit = 50 } = {}) {
    return this.find()
      .sort({ createdAt: -1 })
      .skip(+skip)
      .limit(+limit)
      .populate('level')
      .populate('quests.currentQuest')
      .populate('items.currentItem')
      .populate('skins.element')
      .exec();
  }
};

/**
 * @typedef User
 */
module.exports = mongoose.model('User', UserSchema);
