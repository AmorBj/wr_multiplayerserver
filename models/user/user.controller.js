const User = require('./user.model');
const mongoose = require('mongoose');

async function loadbyUsername(username){
  return await User.findOne({"username":username})
                  .populate('level')
                  .populate('quests.currentQuest')
                  .populate('items.currentItem')
                  .populate('skins.element')
                  .catch(err => console.log(err))
}

async function UserFind(data){
  return await
      User.findOne({ _id : data.id})
          .populate('level')
          .populate('quests.currentQuest')
          .populate('items.currentItem')
          .populate('skins.element')
          .exec()
}

async function UserUpdate(data){
  User.update({_id:currentUser._id},data,function(err,doc){
      if (err) console.log(err)
      else{
          console.log(data._id +" has been updadted")
      }
    })
}


module.exports = {  UserFind, UserUpdate ,loadbyUsername };
