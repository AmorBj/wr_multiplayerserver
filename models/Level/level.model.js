const Promise = require('bluebird');
const mongoose = require('mongoose');



const LevelSchema = new mongoose.Schema({
    value : Number,
    description :  String,
    initial_Exp: Number,
    max_Exp : Number,
  });

LevelSchema.statics = {
        /**
         * Get user
         * @param {ObjectId} id - The objectId of user.
       
         */
        get(id) {
        return this.findById(id)
            .exec()
            .then((level) => {
            if (level) {
                return level;
            }
           
            });
        },
    
        /**
         * List users in descending order of 'createdAt' timestamp.
         * @param {number} skip - Number of users to be skipped.
         * @param {number} limit - Limit number of users to be returned.
         * @returns {Promise<level[]>}
         */
        list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ level: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec();
        },
    }

  module.exports = mongoose.model('Level', LevelSchema);