const Level = require('./level.model');
const mongoose = require('mongoose');

async function createNextLevel(data){
    let newLevel = new Level()
    newLevel.value = data.value + 1;
    newLevel.initial_Exp = data.max_Exp
    newLevel.max_Exp = data.max_Exp * 2;
    return await newLevel.save()
}


module.exports = {  FindNextLevel,createNextLevel };
