const Promise = require('bluebird');
const mongoose = require('mongoose');


const SkinSchema = new mongoose.Schema({
    name: String,
    description: String,
    price : Number ,
    isDefault: {
        type: Boolean,
        default: false
    },

    createdAt: {
        type : Date,
        default: Date.now
    },
    
  });


  SkinSchema.statics = {
        /**
         * Get skin
         * @param {ObjectId} id - The objectId of skins.
         */
        get(id) {
        return this.findById(id)
            .exec()
            .then((skin) => {
            if (skin) {
                return skin;
            }

            });
        },
    
        /**
         * List skins in descending order of 'createdAt' timestamp.
         * @param {number} skip - Number of skins to be skipped.
         * @param {number} limit - Limit number of skins to be returned.
         * @returns {Promise<Skin[]>}
         */
        list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec();
        },
    }

  module.exports = mongoose.model('Skin', SkinSchema);