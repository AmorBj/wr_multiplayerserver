const Promise = require('bluebird');
const mongoose = require('mongoose');



const ItemSchema = new mongoose.Schema({
    name: String,
    description :  String,
    price: Number,
    initialPower: Number,
    maxPower:Number,
    type: String, 

    createdAt :{
        type: Date ,
        default :Date.now
    }
  });



  ItemSchema.statics = {
        /**
         * Get quest
         * @param {ObjectId} id - The objectId of quest.
       
         */
        get(id) {
        return this.findById(id)
            .exec()
            .then((item) => {
            if (item) {
                return item;
            }
           
            });
        },
    
        /**
         * List quests in descending order of 'createdAt' timestamp.
         * @param {number} skip - Number of quests to be skipped.
         * @param {number} limit - Limit number of quests to be returned.
         * @returns {Promise<Item[]>}
         */
        list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .skip(+skip)
            .limit(+limit)
            .exec();
        },
    }

  module.exports = mongoose.model('Item', ItemSchema);