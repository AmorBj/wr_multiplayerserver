const Promise = require('bluebird');
const mongoose = require('mongoose');


const ShopSchema = new mongoose.Schema({
    
    items :[
        {
            item: { 
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Item'
            },
            discount :{
              type : Number ,
              default: 0  
            }
        }
    ],
    skins :[
        {
            skin: { 
                type : mongoose.Schema.Types.ObjectId,
                ref : 'Skin'
            },
            discount :{
              type : Number ,
              default: 0  
            }
        }
    ],
    createdAt: {
        type : Date,
        default: Date.now
    },
    endAt:{
        type : Date,
        default: Date.now() + 24*60*60*1000
    },
    
  });


  ShopSchema.statics = {
   
        get(id) {
        return this.findById(id)
            .populate('items.item').populate('skins.skin')
            .exec()
            .then((shop) => {
            if (shop) {
                return shop;
            }
            });
        },
    
        /**
         * List shops in descending order of 'createdAt' timestamp.
         * @param {number} skip - Number of shops to be skipped.
         * @param {number} limit - Limit number of shops to be returned.
         * @returns {Promise<Shop[]>}
         */
        list({ skip = 0, limit = 50 } = {}) {
        return this.find()
            .populate('items.item').populate('skins.skin')
            .sort({ createdAt: -1 })
            .skip(+skip)
            .limit(+limit)
            .exec();
        },
    }

  module.exports = mongoose.model('Shop', ShopSchema);