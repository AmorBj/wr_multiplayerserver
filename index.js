
// console.log(" Socket server started on "+config.socketport)

const server = require('http').createServer()
const io = require('socket.io')(server); 
require('dotenv').config()

const mongoose = require('mongoose')
const redis = require('redis');
const { promisify } = require('util');

const client = redis.createClient(process.env.REDIS_URL,{
    no_ready_check: true,
    auth_pass: process.env.REDIS_PASSWORD,  
});
const getAsync = promisify(client.get).bind(client);
client.flushall('ASYNC', function(err ,success){
    if (err) console.log(err);
})
//
const UserCntr = require ('./models/user/user.controller');
const Util = require ('./Utils/index');

const Player = require ('./ObjectClasses/Player');
const Room = require ('./ObjectClasses/Room');
const RaceData = require ('./ObjectClasses/RaceData');
const SpawnableObject = require ('./ObjectClasses/SpawnableObject');
const Result = require ('./ObjectClasses/Result');


const nubmerOfPlayersPerGame = process.env.PLAYERS_PER_GAME;


let players = [];
let sockets =[];

let playersWaiting = [];
let rooms =[];

let spawnableObjects= [];


mongoose.connect(process.env.MONGO_HOST, { useNewUrlParser: true } , function (err, db) {
    if (err) throw 'Error connecting to database - ' + err;
});

client.on("error", function (err) {
    console.log("Redis Error " + err);
});
client.on("connect", function(){
    console.log("connected to Redis !")
})



io.on('connection', function (socket) {
    console.log("Connection made !");
    // we need to create a room here , then wait the player to start match making
    // we need also to save room Id for later purposes
    socket.emit("whoAreYou");
    let thisPlayer;
    let myRoom = new Room() ;

    socket.on("whoAmI", async function(data){
        rooms[myRoom.id] = myRoom;
        //*********************** */
        thisPlayer = new Player();
        thisPlayer.id = data.id;
        thisPlayer.username = data.username;
        let result = await getAsync(thisPlayer.id).then(function(currentRes) {
            return JSON.parse(currentRes); 
        }); 
        
        if (result == null ){
            let res = await UserCntr.UserFind(thisPlayer)
            thisPlayer.dBUser = res;
            socket.emit("register", thisPlayer)
            thisPlayer.status = 'online';
            socket.leaveAll();
            socket.join(myRoom.id,function(){}); // joining my room
            players[thisPlayer.id] = thisPlayer;
            sockets[thisPlayer.id] = socket;    
            client.set(thisPlayer.id,JSON.stringify(thisPlayer).toString(),redis.print);
        }else{
                thisPlayer.dBUser = result;
                socket.emit("register", thisPlayer)
                thisPlayer.status = 'online';
                socket.leaveAll();
                socket.join(myRoom.id,function(){}); // joining my room
                players[thisPlayer.id] = thisPlayer;
                sockets[thisPlayer.id] = socket;
         }  
    })
    //////////////////////////////////
    // client.set(thisPlayer.id,JSON.stringify(thisPlayer).toString(),redis.print);
    // getAsync(thisPlayer.id).then(function(res) {
    //     console.log(JSON.parse(res)); // => 'bar'
    // });
    //////////////////////////////////
    // working with menu version 2
    socket.on('IMReady',function(data){
        //telling everyone in this room I am ready include me   
        thisPlayer.username = data.username;
        thisPlayer.isAlive =true;
        thisPlayer.racedata = new RaceData()
        thisPlayer.status = 'ready';
        socket.emit('IMReady',thisPlayer);
        // if (io.nsps["/"].adapter.rooms[myRoom.id].length > 1){  
        if (myRoom.numberOfPlayers > 1){   
            socket.to(myRoom.id).emit('FriendIsReady',thisPlayer);
        } 
        myRoom.numberOfPlayersReady ++;
        thisPlayer.isReady = true;

        console.log("this player is ready "+ thisPlayer.id);
        
        if (myRoom.numberOfPlayersReady === myRoom.numberOfPlayers){
            console.log('all players are ready in this room '+ myRoom.id);
            // here we will add the team on waiting queu
            // it need to be the whole team together !!!
            if (myRoom.numberOfPlayers >= nubmerOfPlayersPerGame){
                // **** match make directly 
                socket.to(myRoom.id).emit("startPLaying")
                socket.emit("startPlaying")
                console.log("startPlaying "+ myRoom.id)
            }else {
                let numberOfPlayersWAiting = myRoom.numberOfPlayers;
                let i=0;
                let nextRoom =playersWaiting[i];
                console.log("nextRoom : " ,nextRoom);
                
                let roomsToMatchMadeWith = [];
                while (nextRoom !== undefined && numberOfPlayersWAiting < nubmerOfPlayersPerGame ){
                    let x = numberOfPlayersWAiting + nextRoom.numberOfPlayers;
                    if ( x < nubmerOfPlayersPerGame){
                        roomsToMatchMadeWith[i] = nextRoom;
                        console.log("adding this room "+nextRoom.id +" to play with" )
                    }else if ( x == nubmerOfPlayersPerGame){
                        console.log("rooms to made match with = " , roomsToMatchMadeWith)
                        roomsToMatchMadeWith.forEach(element => {
                            playersWaiting.shift();
                            console.log("emitting join me to ",element);
                            socket.to(element.id).emit("joinMe",myRoom);
                        });
                        playersWaiting.shift();
                        socket.to(nextRoom.id).emit("joinMe",myRoom);
                        //delete  playersWaiting[i];
                        console.log("number = "+x+" players per game and wait list = " , playersWaiting)
                    }
                    numberOfPlayersWAiting += nextRoom.numberOfPlayers;
                    i++;
                    nextRoom = playersWaiting[i];
                }
                if (numberOfPlayersWAiting < nubmerOfPlayersPerGame){
                    console.log(myRoom.id +" is waiting for matching !");
                    socket.to(myRoom.id).emit("waiting");
                    socket.emit("waiting");
                    playersWaiting.unshift(myRoom); // added the whole room so we can find it later to make the match and verify the number of players
                }
            }
        }
         
     
    })

    socket.on('joinMyLobby',function(data){
        socket.leaveAll()
        myRoom = rooms[data.id]
        socket.join(data.id , function(){
            myRoom.numberOfPlayers++;
        })
    });

    socket.on('CancelReady',function(data){
        //telling everyone in this room I am ready include me      
        myRoom.numberOfPlayersReady--;
        thisPlayer.isReady =false;
        thisPlayer.status = 'online';
        
        playersWaiting.splice(playersWaiting.indexOf(myRoom),1); // ther may be error later
        socket.to(room.id).emit('FriendCanceledReady',thisPlayer); 
        socket.emit('CancelReady',thisPlayer);
    })

    socket.on('joinRoom',function(data){
        thisPlayer.username = data.username;
        console.log(data.username +" JoiningRoom :" + data.id);
        socket.join(data.id , function(){
            delete rooms[myRoom.id];
            myRoom = rooms[data.id];
            console.log("   ===> "+thisPlayer.username+" has joined "+data.id)
            if (io.nsps["/"].adapter.rooms[myRoom.id].length >= nubmerOfPlayersPerGame){
                socket.to(myRoom.id).emit('startPlaying')
                socket.emit('startPlaying')
            }
        });
    })

    socket.on('readyToSpawn',function(data){
        myRoom.numberOfPlayersReadyToBeSpawned++;
        thisPlayer.skin = data.element;
        thisPlayer.status = 'inGame';
        console.log("player ready to play !!" , thisPlayer)
        if (myRoom.numberOfPlayersReadyToBeSpawned >= nubmerOfPlayersPerGame){
            myRoom.numberOfPlayersReadyToBeSpawned = 0;
            socket.to(myRoom.id).emit('readyToSpawn',thisPlayer) 
            socket.emit('readyToSpawn',thisPlayer) 
        }
    })

    socket.on('startCountDown',function(data){
        myRoom.numberOfPlayersSpawned++;

        if (myRoom.numberOfPlayersSpawned >= nubmerOfPlayersPerGame){
            myRoom.numberOfPlayersSpawned = 0;
            let time=3;
            let timer = setInterval(function(){
                if (time <= 0){
                    socket.to(myRoom.id).emit('run')  
                    socket.emit('run') 
                    myRoom.startGame(socket)
                    clearInterval(timer);
                }else{
                    socket.to(myRoom.id).emit('countDown',{time : time}) 
                    socket.emit('countDown',{time : time}) 
                }
                time--;
            }, 1000);
        }
    })

    socket.on('matchMaking',function(data){
        socket.to(myRoom.id).emit('spawn',thisPlayer) // Telling everyone that this player is Spawned
        socket.emit('spawn',thisPlayer) // spawn the Play Himself
        console.log("match made for "+ thisPlayer.username);
    })    
    
    socket.on('leaveRoom',async function(data){
        if ( thisPlayer.isAlive){
            thisPlayer.isAlive =false;
            thisPlayer.racedata.raceTime = (Date.now() - myRoom.time) /1000;
            console.log(" Race Time :" + thisPlayer.racedata.raceTime)
            thisPlayer.racedata.rank = (nubmerOfPlayersPerGame - myRoom.result.playersDead);
            myRoom.result.playersDead ++ ;
            thisPlayer.racedata.exp = await Util.CalculateExp(thisPlayer.racedata, thisPlayer.isAlive);
            thisPlayer.dBUser.experience += thisPlayer.racedata.exp;
            if (  myRoom.result.playersDead  + myRoom.result.playersFinished < nubmerOfPlayersPerGame)
            {
                socket.to(myRoom.id).emit("playerDead",thisPlayer);
            }else{
                console.log("race finished")
                myRoom.stopTimer();
            }
        }
         socket.to(myRoom.id).emit('playerLeft',thisPlayer)
        socket.emit('playerLeft',thisPlayer)
        
        console.log(thisPlayer.username +" left the room ")
        
        socket.leave(myRoom.id , function(){
            myRoom.numberOfPlayers -- ;
            if (thisPlayer.isReady){
                myRoom.numberOfPlayersReady--;
                thisPlayer.isReady =false;
            }
            if (myRoom.numberOfPlayers < 1){
                delete rooms[myRoom.id];
            }
            thisPlayer.status = 'online';
            myRoom = new Room();
            rooms[myRoom.id] = myRoom;
            socket.join(myRoom.id,function(){}); // joining my room
            console.log(thisPlayer.username +" joined a new room " + myRoom.id)
            
        });
    });

    // recieving player Position and telling others about it 
    socket.on('updatePosition', function(data){
        thisPlayer.position.x = data.position.x;
        thisPlayer.position.y = data.position.y;
        thisPlayer.faceRight = data.faceRight;
        socket.to(myRoom.id).emit('updatePosition',thisPlayer);
    })

    // recieving if player Animating and telling others about it 
    socket.on('updateAnimation', function(data){
        thisPlayer.animData.speed = data.speed;
        thisPlayer.animData.isGrounded = data.isGrounded;
        thisPlayer.animData.isJumping = data.isJumping;
       
        socket.to(myRoom.id).emit('updateAnimation',thisPlayer);
    })
    
    socket.on("updateHealth",async function(data){
        thisPlayer.healthAndShield.health = data.health;
        thisPlayer.healthAndShield.shield = data.shield;
        thisPlayer.healthAndShield.maxHealth = data.maxHealth;
        thisPlayer.healthAndShield.maxShield = data.maxShield;

        console.log("data after health changed : ",data)

         if (thisPlayer.isAlive){
            if (thisPlayer.healthAndShield.health <= 0 ){
                thisPlayer.isAlive =false;
                thisPlayer.racedata.raceTime = (Date.now() - myRoom.time) /1000;
                thisPlayer.racedata.rank = (nubmerOfPlayersPerGame - myRoom.result.playersDead);
                myRoom.result.playersDead ++ ;
                if (myRoom.playersDead + myRoom.playersFinished >= nubmerOfPlayersPerGame){
                    myRoom.stopTimer()
                }
                thisPlayer.racedata.exp = await Util.CalculateExp(thisPlayer.racedata, thisPlayer.isAlive);
                thisPlayer.dBUser.experience += thisPlayer.racedata.exp;
                //player is Dead we should notice everyone else !
                socket.to(myRoom.id).emit("playerDead",thisPlayer);
                socket.emit("playerDead",thisPlayer);
                console.log("player Is dead ", thisPlayer)
            } else{
                socket.to(myRoom.id).emit("updateHealth",thisPlayer);
                socket.emit("updateHealth",thisPlayer);
                // tell everybody this player health
            }
        }
       
    })

    socket.on("finishedRace", async function(data){
        myRoom.result.playersFinished ++;
        thisPlayer.racedata.rank = myRoom.result.playersFinished;
        thisPlayer.racedata.raceTime = (Date.now() - myRoom.time) /1000;
        thisPlayer.racedata.exp = await Util.CalculateExp(thisPlayer.racedata, thisPlayer.isAlive);
        thisPlayer.isAlive = false;
        thisPlayer.dBUser.experience += thisPlayer.racedata.exp;
        if (  myRoom.result.playersDead  + myRoom.result.playersFinished >= nubmerOfPlayersPerGame)
        {
            console.log("race finished")
            myRoom.stopTimer();
        }
        socket.to(myRoom.id).emit('finishedRace',thisPlayer)
        socket.emit('finishedRace',thisPlayer)
        
        console.log('someone has finished the race !'+ thisPlayer.dBUser.experience +" in server time "+ myRoom.time)
    })

    socket.on('gotHitted',function(data){
        // let playerHitted = players[data.playerId] // this player was hitted by something
        console.log("hit data : " ,data);
        let interactableObject = spawnableObjects[data.interactableObjectID] // this something hitted that previous player 
        let ownerObject = players[interactableObject.ownerId] // this player own that something which hit the other player
        ownerObject.racedata.damageDone += interactableObject.amount; 
    })

    socket.on('spawnInteractableObject',function(data){
        let spawnableObject = new SpawnableObject();
        spawnableObject.name = data.name;
        spawnableObject.ownerId = thisPlayer.id;
        spawnableObject.position.x = data.position.x;
        spawnableObject.position.y = data.position.y;
        
        spawnableObject.rotation.x = data.rotation.x;
        spawnableObject.rotation.y = data.rotation.y;
        spawnableObject.amount = data.amount;

        thisPlayer.racedata.items.push(data.name)
        
        switch (spawnableObject.name){
            case 'Bomb':
                socket.to(myRoom.id).emit("spawnInteractableObject",spawnableObject);
                socket.emit("spawnInteractableObject",spawnableObject);
                spawnableObjects[spawnableObject.id] = spawnableObject;
                break; 
            
            case 'Heal':
                if( thisPlayer.healthAndShield.health < thisPlayer.healthAndShield.maxHealth){
                    let healthNeeded = thisPlayer.healthAndShield.maxHealth - thisPlayer.healthAndShield.health;
                    if (healthNeeded >= spawnableObject.amount){
                        thisPlayer.racedata.healDone += spawnableObject.amount;
                        thisPlayer.healthAndShield.health += spawnableObject.amount;
                    }else{
                        thisPlayer.racedata.healDone += healthNeeded;
                        thisPlayer.healthAndShield.health += healthNeeded;
                    }
                    socket.to(myRoom.id).emit("updateHealth",thisPlayer);
                    socket.emit("updateHealth",thisPlayer);
                }
                break;
            
            case 'Shield':
                if ( thisPlayer.healthAndShield.shield < thisPlayer.healthAndShield.maxShield){
                    let shieldNeeded = thisPlayer.healthAndShield.maxShield - thisPlayer.healthAndShield.shield;
                    if (shieldNeeded >= spawnableObject.amount){
                        thisPlayer.racedata.shieldDone += spawnableObject.amount;
                        thisPlayer.healthAndShield.shield += spawnableObject.amount;
                    }else{
                        thisPlayer.racedata.shieldDone += shieldNeeded;
                        thisPlayer.healthAndShield.shield += shieldNeeded;
                    }
                    socket.to(myRoom.id).emit("updateHealth",thisPlayer);
                    socket.emit("updateHealth",thisPlayer);
                }
               // thisPlayer.racedata.shieldDone += spawnableObject.amount;
                break;
            
            default : 
                console.log("This name of interactable object is undefined please check your code !")
                break;
        }
    })

    socket.on('destroyInteractableObject',function(data){
        if (spawnableObjects[data.id] != undefined){
            delete spawnableObjects[data.id]
            console.log("Object destroyed " + data.id);
            // need to telle everyone about this !
        }
    })

    socket.on('disconnect', async function(data){
        delete players[thisPlayer.id]
        delete sockets[thisPlayer.id]
        socket.to(myRoom.id).emit('playerLeft',thisPlayer);
        
        console.log("player has disconnected ", thisPlayer)
        // ********
        socket.leave(myRoom.id , async function(){
            myRoom.numberOfPlayers -- ;
            if (thisPlayer.isReady){
                myRoom.numberOfPlayersReady--;
                // thisPlayer.isReady =false;
            }
            if (myRoom.numberOfPlayers < 1){
                delete rooms[myRoom.id];
            }else if (thisPlayer.status == 'inGame'){
                thisPlayer.isAlive =false;
                thisPlayer.racedata.raceTime = (Date.now() - myRoom.time) /1000;
                thisPlayer.racedata.rank = (nubmerOfPlayersPerGame - myRoom.result.playersDead);
                myRoom.result.playersDead ++ ;
                thisPlayer.racedata.exp =await Util.CalculateExp(thisPlayer.racedata, thisPlayer.isAlive);
                thisPlayer.dBUser.experience += thisPlayer.racedata.exp;
                if (myRoom.playersDead +myRoom.playersFinished >= nubmerOfPlayersPerGame){
                    myRoom.stopTimer();
                }
                //player is Dead we should notice everyone else !
                socket.to(myRoom.id).emit("playerDead",thisPlayer);
            }
            // let result = await getAsync(thisPlayer.id).then(function(currentRes) {
            //     return JSON.parse(currentRes); 
            // });
            // if (!Util.Compare(thisPlayer.dBUser,result)){
            //     UserUpdate(thisPlayer.dBUser)
            // }
        });
    })

    //* ********************** */
});


server.listen(process.env.PORT || 4050);
console.log("serve has started on "+ process.env.PORT )