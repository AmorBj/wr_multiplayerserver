Compare = function (obj1, obj2) {
	//Loop through properties in object 1
	for (var p in obj1) {
		//Check property exists on both objects
		if (obj1.hasOwnProperty(p) !== obj2.hasOwnProperty(p)) return false;
 
		switch (typeof (obj1[p])) {
			//Deep compare objects
			case 'object':
				if (!Object.compare(obj1[p], obj2[p])) return false;
				break;
			//Compare function code
			case 'function':
				if (typeof (obj2[p]) == 'undefined' || (p != 'compare' && obj1[p].toString() != obj2[p].toString())) return false;
				break;
			//Compare values
			default:
				if (obj1[p] != obj2[p]) return false;
		}
	}
 
	//Check object 2 for any extra properties
	for (var p in obj2) {
		if (typeof (obj1[p]) == 'undefined') return false;
	}
	return true;
};


async function CalculateExp(data,isAlive){
    let HealShieldexp = (data.healDone + data.shieldDone) * 0.1;
    let damageExp = data.damageDone * 0.2;
    let rankExp = (data.rank -1) * 5;
    let timeExp = 0;
    if (isAlive){
        timeExp = 100 + Math.exp(5.5 -(data.raceTime/25))
    }else{
        timeExp = 100 - Math.exp(4.6 -(data.raceTime/25))  
    }
    let total =  HealShieldexp + damageExp +timeExp - rankExp;
   
    return total;
}


async function SkinChanged(data ,skin){
	data.forEach(element => {
		if (element.in_use){
			element.in_use = false;
		}
		if (element.element._id == skin._id){
			element.in_use =true;
		}
	});
}

module.exports = { Compare , CalculateExp , SkinChanged}